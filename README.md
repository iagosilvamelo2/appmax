## Ambiente

PHP 7.3.11

Laravel 7.3.0

Mysql 5.7

Docker 19.03.6

Docker-compose 1.21.0

## Iniciando serviço

Instalando serviços da aplicação

```
$ docker-compose up -d --build
$ composer install
$ php artisan migrate
$ php artisan db:seed
```

Com os serviços instalados, podemos gerar os tokens do aplicativo e do passport
```
$ php artisan key:generate
$ php artisan passport:install
```

Com as chaves do passport geradas, armazenar para utilizar nas requisições de API

Para iniciar o servidor web, basta rodar o seguinte comando
```
$ php artisan serve
```
Port padrão irá iniciar o serviço em "http://localhost:8000"

## Acesso

#### Web

- Acessar o endereço do servidor "http://localhost:8000"

- Registrar um novo usuário

- Efetuar login

#### Api

Com usuário já registrado, efetue a autenticação no endpoint "http://localhost:8000/oauth/token" como POST passando os seguintes parâmetros.

```
{
	"grant_type": "password",
	"client_id": 2, // Id gerado pelo passport, pode ser diferente
	"client_secret": "xxxxxxxx", // Secret referente ao id informado
	"username": "Email cadastrado",
	"password": "Senha cadastrada"
}
```

Com tudo informado corretamente, deve retornar algo assim

```
{
  "token_type": "Bearer",
  "expires_in": 31536000,
  "access_token": "",
  "refresh_token": ""
}
```

Em todas as requisições a API deve ser passado um header "Authorization" com o valor "Bearer 'conteúdo do access_token'".

## Endpoints

#### baixar-produtos

Deve ser feito por requisiao GET, passando id do produto e quantidade.
```
http://localhost:8000/api/baixar-produtos?id=1&quantidade=5
```

#### adicionar-produtos

Deve ser feito por requisiao GET, passando id do produto e quantidade.
```
http://localhost:8000/api/adicionar-produtos?id=1&quantidade=5
```
