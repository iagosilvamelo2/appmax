<?php

namespace App\Http\Controllers\Produto;

use App\Http\Controllers\Controller;
use App\Http\Models\Produto\Produto;
use Illuminate\Http\Request;

class ProdutoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json( Produto::all() );
    }

    /**
     * Display a listing of the resource whit pagination.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function paginate(Request $request)
    {
        $order = ( !empty( $request->get('order') )) ? $request->get('order') : "asc";
        $by    = ( !empty( $request->get('by')    )) ? $request->get('by')    : "id";
        $rows  = ( !empty( $request->get('rows')  )) ? $request->get('rows')  : 10;

        return response()->json( Produto::orderBy($by, $order)->paginate($rows) );
    }

    /**
     * Display the specified resource.
     *
     * @param Integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return response()->json( Produto::find($id) );
    }

    /**
     * Creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $error = $this->verify_inputs( $request->all() );

        if ( !empty($error) )
            return response()->json( $error, 400 );

            return $request->all();

        // else return response()->json( Produto::create($request->all()) );
    }

    /**
     * Editing the specified resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param Integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request, $id)
    {
        $error   = $this->verify_inputs( $request->all() );
        $produto = Produto::find($id);

        if ( !empty($error) )
            return response()->json( $error, 400 );

        if ( empty($produto) )
            return response()->json( "Produto não encontrado", 400 );

        $produto->sku                  = $request->input('sku');
        $produto->nome                 = $request->input('nome');
        $produto->descricao            = $request->input('descricao');
        $produto->preco                = $request->input('preco');
        $produto->produto_tamanho_id   = $request->input('produto_tamanho_id');
        $produto->produto_categoria_id = $request->input('produto_categoria_id');
        $produto->produto_cor_id       = $request->input('produto_cor_id');
        $produto->produto_estoque_id   = $request->input('produto_estoque_id');
        $produto->status               = $request->input('status');
        $produto->updated_to           = $request->input('updated_to');
        $produto->save();

        return response()->json( Produto::find($produto->id) );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy( $id )
    {
        $produto = Produto::find($id);

        if ( empty($produto) )
            return response()->json( "Produto não encontrado", 400 );

        $produto->delete();
        return response()->json( "Produto deletado com sucesso" );
    }

    /**
     * Verify inputs to creating a new resource.
     *
     * @param Array $inputs
     * @return Array
     */
    private function verify_inputs( $inputs )
    {
        $error = [];

        if ( !$inputs['sku'] )
            array_push( $error, "SKU não pode ser vazio." );

        if ( !$inputs['nome'] )
            array_push( $error, "Nome não pode ser vazio." );

        if ( !$inputs['produto_tamanho_id'] )
            array_push( $error, "Tamanho não pode ser vazio." );

        if ( !$inputs['produto_categoria_id'] )
            array_push( $error, "Categoria não pode ser vazio." );

        if ( !$inputs['produto_cor_id'] )
            array_push( $error, "Cor não pode ser vazio." );

        if ( !$inputs['produto_estoque_id'] )
            array_push( $error, "Estoque não pode ser vazio." );

        if ( !$inputs['status'] )
            array_push( $error, "status não pode ser vazio." );

        if ( !$inputs['updated_to'] )
            array_push( $error, "updated_to não pode ser vazio." );

        return $error;
    }
}
