<?php

namespace App\Http\Controllers\Produto;

use App\Http\Controllers\Controller;
use App\Http\Models\Produto\Cor;
use Illuminate\Http\Request;

class ProdutoCorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json( Cor::all() );
    }

    /**
     * Display a listing of the resource whit pagination.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function paginate(Request $request)
    {
        $order = ( !empty( $request->get('order') )) ? $request->get('order') : "asc";
        $by    = ( !empty( $request->get('by')    )) ? $request->get('by')    : "id";
        $rows  = ( !empty( $request->get('rows')  )) ? $request->get('rows')  : 10;

        return response()->json( Cor::orderBy($by, $order)->paginate($rows) );
    }

    /**
     * Display the specified resource.
     *
     * @param Integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return response()->json( Cor::find($id) );
    }

    /**
     * Creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $error = $this->verify_inputs( $request->all() );

        if ( !empty($error) )
            return response()->json( $error, 400 );

        else return response()->json( Cor::create($request->all()) );
    }

    /**
     * Editing the specified resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param Integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request, $id)
    {
        $error   = $this->verify_inputs( $request->all() );
        $cor = Cor::find($id);

        if ( !empty($error) )
            return response()->json( $error, 400 );

        if ( empty($cor) )
            return response()->json( "Cor não encontrada", 400 );

        $cor->name      = $request->input('name');
        $cor->descricao = $request->input('descricao');
        $cor->save();

        return response()->json( $cor );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy( $id )
    {
        $cor = Cor::find($id);

        if ( empty($cor) )
            return response()->json( "Cor não encontrada", 400 );

        $cor->delete();
        return response()->json( "Cor deletada com sucesso" );
    }

    /**
     * Verify inputs to creating a new resource.
     *
     * @param Array $inputs
     * @return Array
     */
    private function verify_inputs( $inputs )
    {
        $error = [];

        if ( !$inputs['nome'] )
            array_push( $error, "Nome não pode ser vazio." );

        return $error;
    }
}
