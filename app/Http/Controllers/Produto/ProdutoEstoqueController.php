<?php

namespace App\Http\Controllers\Produto;

use App\Http\Controllers\Controller;
use App\Http\Models\Produto\Estoque;
use Illuminate\Http\Request;

class ProdutoEstoqueController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json( Estoque::all() );
    }

    /**
     * Display a listing of the resource whit pagination.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function paginate(Request $request)
    {
        $order = ( !empty( $request->get('order') )) ? $request->get('order') : "asc";
        $by    = ( !empty( $request->get('by')    )) ? $request->get('by')    : "id";
        $rows  = ( !empty( $request->get('rows')  )) ? $request->get('rows')  : 10;

        return response()->json( Estoque::orderBy($by, $order)->paginate($rows) );
    }

    /**
     * Display the specified resource.
     *
     * @param Integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return response()->json( Estoque::find($id) );
    }

    /**
     * Creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $error = $this->verify_inputs( $request->all() );

        if ( !empty($error) )
            return response()->json( $error, 400 );

        else return response()->json( Estoque::create($request->all()) );
    }

    /**
     * Editing the specified resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param Integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request, $id)
    {
        $error   = $this->verify_inputs( $request->all() );
        $estoque = Estoque::find($id);

        if ( !empty($error) )
            return response()->json( $error, 400 );

        if ( empty($estoque) )
            return response()->json( "Estoque não encontrado", 400 );

        $estoque->nome      = $request->input('nome');
        $estoque->descricao = $request->input('descricao');
        $estoque->atual     = $request->input('atual');
        $estoque->minimo    = $request->input('minimo');
        $estoque->maximo    = $request->input('maximo');
        $estoque->save();

        return response()->json( $estoque );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy( $id )
    {
        $estoque = Estoque::find($id);

        if ( empty($estoque) )
            return response()->json( "Estoque não encontrado", 400 );

        $estoque->delete();
        return response()->json( "Estoque deletado com sucesso" );
    }

    /**
     * Reduces items.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function baixar( Request $request )
    {
        $produto_id = $request->get('id');
        $quantidade = $request->get('quantidade');

        if ( !isset($produto_id) || !isset($quantidade) )
            return response()->json("Informar produto_id e quanditade.", 400);
        $produto = \App\Http\Models\Produto\Produto::find($produto_id);

        $estoque = Estoque::find($produto->produto_estoque_id);

        if ( $estoque->atual < $quantidade )
            return response()->json( "Você não pode baixar mais produtos do que há em estoque", 400 );

        $estoque->atual -= $quantidade;
        $estoque->save();

        return response()->json( $estoque );
    }

    /**
     * Increase items.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function adicionar( Request $request )
    {
        $produto_id = $request->get('id');
        $quantidade = $request->get('quantidade');

        if ( !isset($produto_id) || !isset($quantidade) )
            return response()->json("Informar produto_id e quanditade.", 400);
        $produto = \App\Http\Models\Produto\Produto::find($produto_id);
        $estoque = Estoque::find($produto->produto_estoque_id);

        $estoque->atual += $quantidade;
        $estoque->save();

        return response()->json( $estoque );
    }

    /**
     * Verify inputs to creating a new resource.
     *
     * @param Array $inputs
     * @return Array
     */
    private function verify_inputs( $inputs )
    {
        $error = [];

        if ( !$inputs['nome'] )
            array_push( $error, "Nome não pode ser vazio." );

        return $error;
    }
}
