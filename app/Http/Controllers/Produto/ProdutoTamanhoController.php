<?php

namespace App\Http\Controllers\Produto;

use App\Http\Controllers\Controller;
use App\Http\Models\Produto\Tamanho;
use Illuminate\Http\Request;

class ProdutoTamanhoController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json( Tamanho::all() );
    }

    /**
     * Display a listing of the resource whit pagination.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function paginate(Request $request)
    {
        $order = ( !empty( $request->get('order') )) ? $request->get('order') : "asc";
        $by    = ( !empty( $request->get('by')    )) ? $request->get('by')    : "id";
        $rows  = ( !empty( $request->get('rows')  )) ? $request->get('rows')  : 10;

        return response()->json( Tamanho::orderBy($by, $order)->paginate($rows) );
    }

    /**
     * Display the specified resource.
     *
     * @param Integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        return response()->json( Tamanho::find($id) );
    }

    /**
     * Creating a new resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request)
    {
        $error = $this->verify_inputs( $request->all() );

        if ( !empty($error) )
            return response()->json( $error, 400 );

        else return response()->json( Tamanho::create($request->all()) );
    }

    /**
     * Editing the specified resource.
     *
     * @param \Illuminate\Http\Request $request
     * @param Integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request, $id)
    {
        $error   = $this->verify_inputs( $request->all() );
        $tamanho = Tamanho::find($id);

        if ( !empty($error) )
            return response()->json( $error, 400 );

        if ( empty($tamanho) )
            return response()->json( "Tamanho não encontrado", 400 );

        $tamanho->name      = $request->input('name');
        $tamanho->descricao = $request->input('descricao');
        $tamanho->save();

        return response()->json( $tamanho );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Integer $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy( $id )
    {
        $tamanho = Tamanho::find($id);

        if ( empty($tamanho) )
            return response()->json( "Tamanho não encontrado", 400 );

        $tamanho->delete();
        return response()->json( "Tamanho deletado com sucesso" );
    }

    /**
     * Verify inputs to creating a new resource.
     *
     * @param Array $inputs
     * @return Array
     */
    private function verify_inputs( $inputs )
    {
        $error = [];

        if ( !$inputs['nome'] )
            array_push( $error, "Nome não pode ser vazio." );

        return $error;
    }
}
