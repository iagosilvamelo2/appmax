<?php

namespace App\Http\Models\Produto;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $table = 'produto_categoria';

    protected $primaryKey = 'id';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nome', 'descricao',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
