<?php

namespace App\Http\Models\Produto;

use Illuminate\Database\Eloquent\Model;

class Estoque extends Model
{
    protected $table = 'produto_estoque';

    protected $primaryKey = 'id';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nome', 'descricao', 'atual', 'minimo', 'maximo',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
