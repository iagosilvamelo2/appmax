<?php

namespace App\Http\Models\Produto;

use Illuminate\Database\Eloquent\Model;

class Produto extends Model
{
    protected $table = 'produto';

    protected $primaryKey = 'id';

    public $timestamps = true;

    /**
     * Get the tamanho for produto.
     */
    public function tamanho()
    {
        return $this->hasOne('App\Http\Models\Produto\Tamanho', 'id', 'produto_tamanho_id');
    }

    /**
     * Get the categoria for produto.
     */
    public function categoria()
    {
        return $this->hasOne('App\Http\Models\Produto\Categoria', 'id', 'produto_categoria_id');
    }

    /**
     * Get the cor for produto.
     */
    public function cor()
    {
        return $this->hasOne('App\Http\Models\Produto\Cor', 'id', 'produto_cor_id');
    }

    /**
     * Get the estoque for produto.
     */
    public function estoque()
    {
        return $this->hasOne('App\Http\Models\Produto\Estoque', 'id', 'produto_estoque_id');
    }

    /**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = ['tamanho', 'categoria', 'cor', 'estoque'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'sku', 'nome', 'preco', 'descricao', 'tamanho_id', 'categoria_id', 'cor_id', 'estoque_id', 'status', 'updated_to',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
