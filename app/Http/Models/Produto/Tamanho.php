<?php

namespace App\Http\Models\Produto;

use Illuminate\Database\Eloquent\Model;

class Tamanho extends Model
{
    protected $table = 'produto_tamanho';

    protected $primaryKey = 'id';

    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'nome', 'descricao',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];
}
