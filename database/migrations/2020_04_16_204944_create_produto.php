<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProduto extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produto', function (Blueprint $table) {
            $table->id();
            $table->string('sku', 50);
            $table->string('nome', 250);
            $table->text('descricao')->nullable();
            $table->float('preco', 8, 2)->nullable();
            $table->integer('produto_tamanho_id');
            $table->integer('produto_categoria_id');
            $table->integer('produto_cor_id');
            $table->integer('produto_estoque_id');
            $table->boolean('status');
            $table->string('updated_to', 3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produto');
    }
}
