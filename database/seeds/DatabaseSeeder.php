<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ProdutoTamanhoSeeder::class);
        $this->call(ProdutoCategoriaSeeder::class);
        $this->call(ProdutoCorSeeder::class);
        $this->call(ProdutoEstoqueSeeder::class);
        $this->call(ProdutoSeeder::class);
    }
}
