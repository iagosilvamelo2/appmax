<?php

use Illuminate\Database\Seeder;
use App\Http\Models\Produto\Categoria;

class ProdutoCategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Categoria::truncate();

        Categoria::create([
            'nome'      => 'Vestuario',
            'descricao' => 'Itens de vestuário'
        ]);

        Categoria::create([
            'nome'      => 'Papelaria',
            'descricao' => 'Materiais escolares e de escritório'
        ]);

        Categoria::create([
            'nome'      => 'Cozinha',
            'descricao' => 'Louça, talheres, potes e panelas'
        ]);
    }
}
