<?php

use Illuminate\Database\Seeder;
use App\Http\Models\Produto\Cor;

class ProdutoCorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cor::truncate();

        Cor::create([
            'nome'      => 'Azul',
            'descricao' => 'Cor azul'
        ]);

        Cor::create([
            'nome'      => 'Verde',
            'descricao' => 'Cor verde'
        ]);

        Cor::create([
            'nome'      => 'Vermelho',
            'descricao' => 'Cor vermelha'
        ]);
    }
}
