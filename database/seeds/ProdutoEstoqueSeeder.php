<?php

use Illuminate\Database\Seeder;
use App\Http\Models\Produto\Estoque;

class ProdutoEstoqueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Estoque::truncate();

        Estoque::create([
            'nome'      => 'Blusas XYZ',
            'descricao' => 'Controle de estoque para blusas femininas XYZ',
            'atual'     => 10,
            'minimo'    => 5,
            'maximo'    => 20
        ]);

        Estoque::create([
            'nome'      => 'Canetas X',
            'descricao' => 'Controle de estoque para canetas X',
            'atual'     => 10,
            'minimo'    => 5,
            'maximo'    => 20
        ]);

        Estoque::create([
            'nome'      => 'Panelas ABC',
            'descricao' => 'Controle de estoque para panelas ABC',
            'atual'     => 10,
            'minimo'    => 5,
            'maximo'    => 20
        ]);
    }
}
