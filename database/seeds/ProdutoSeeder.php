<?php

use Illuminate\Database\Seeder;
use App\Http\Models\Produto\Produto;

class ProdutoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Produto::truncate();

        Produto::create([
            'sku'                  => 'BL-P-VES-VRM',
            'nome'                 => 'Blusa XYZ',
            'preco'                => 14.90,
            'descricao'            => 'Blusa feminina marca XYZ na cor vermelha',
            'produto_tamanho_id'   => 1,
            'produto_categoria_id' => 1,
            'produto_cor_id'       => 3,
            'produto_estoque_id'   => 1,
            'status'               => 1,
            'updated_to'           => 'APP'
        ]);

        Produto::create([
            'sku'                  => 'CA-M-PAP-AZU',
            'nome'                 => 'Caneta X',
            'preco'                => 1.50,
            'descricao'            => 'Caneta da marca X na cor azul',
            'produto_tamanho_id'   => 2,
            'produto_categoria_id' => 2,
            'produto_cor_id'       => 1,
            'produto_estoque_id'   => 2,
            'status'               => 1,
            'updated_to'           => 'APP'
        ]);

        Produto::create([
            'sku'                  => 'PA-G-COZ-VRD',
            'nome'                 => 'Panelas ABC',
            'preco'                => 29.90,
            'descricao'            => 'Panela da marca ABC na cor verde',
            'produto_tamanho_id'   => 3,
            'produto_categoria_id' => 3,
            'produto_cor_id'       => 2,
            'produto_estoque_id'   => 3,
            'status'               => 1,
            'updated_to'           => 'APP'
        ]);
    }
}
