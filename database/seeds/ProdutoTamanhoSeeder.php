<?php

use Illuminate\Database\Seeder;
use App\Http\Models\Produto\Tamanho;

class ProdutoTamanhoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Tamanho::truncate();

        Tamanho::create([
            'nome'      => 'Pequeno',
            'descricao' => 'Tamanho pequeno'
        ]);

        Tamanho::create([
            'nome'      => 'Medio',
            'descricao' => 'Tamanho médio'
        ]);

        Tamanho::create([
            'nome'      => 'Grande',
            'descricao' => 'Tamanho grande'
        ]);
    }
}
