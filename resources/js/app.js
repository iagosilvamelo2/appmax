require('./bootstrap')

window.Vue = require('vue')

import VcProdutos from './components/VcProdutos.vue'

const app = new Vue({
    el: '#app',
    name: 'Appmax',
    components: { VcProdutos }
});
