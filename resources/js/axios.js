import axios from "axios"

const instance = axios.create({ baseURL: '/' })

export default {
	data() { return { instance_params: { headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') } } } },

	methods: {
		async GET(route, foo = "") {
			const response = await instance.get(`${route}/${foo}`, this.instance_params).then(r => r.data);
			return response
		},

		async POST(route, data) {
			const response = await instance.post(route, data, this.instance_params).then(r => r.data);
			return response
		},

		async PUT(route, data) {
			const response = await instance.put(`${route}/${(data.id) ? data.id : data.ID}`, data, this.instance_params).then(r => r.data);
			return response
		},

		async DELETE(route, id) {
			const response = await instance.delete(`${route}/${id}`, this.instance_params, {}).then(r => r.data);
			return response
		}
	}
}
