<?php

$router->group(['prefix' => 'categoria'], function($router) {

    $router->get('/',        "ProdutoCategoriaController@index");
    $router->get('paginate', "ProdutoCategoriaController@paginate");
    $router->get('{id}',     "ProdutoCategoriaController@show");

    $router->post('/',       "ProdutoCategoriaController@create");
    $router->put('{id}',     "ProdutoCategoriaController@edit");
    $router->delete('{id}',  "ProdutoCategoriaController@destroy");

});
