<?php

$router->group(['prefix' => 'cor'], function($router) {

    $router->get('/',        "ProdutoCorController@index");
    $router->get('paginate', "ProdutoCorController@paginate");
    $router->get('{id}',     "ProdutoCorController@show");

    $router->post('/',       "ProdutoCorController@create");
    $router->put('{id}',     "ProdutoCorController@edit");
    $router->delete('{id}',  "ProdutoCorController@destroy");

});
