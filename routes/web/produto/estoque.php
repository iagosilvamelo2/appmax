<?php

$router->group(['prefix' => 'estoque'], function($router) {

    $router->get('/',        "ProdutoEstoqueController@index");
    $router->get('paginate', "ProdutoEstoqueController@paginate");
    $router->get('{id}',     "ProdutoEstoqueController@show");

    $router->post('/',       "ProdutoEstoqueController@create");
    $router->put('{id}',     "ProdutoEstoqueController@edit");
    $router->delete('{id}',  "ProdutoEstoqueController@destroy");

});
