<?php

$params = [
    'prefix'     => 'produto',
    'namespace'  => 'Produto',
    'middleware' => 'auth'
];

$router->group($params, function($router) {

    require __DIR__.'/tamanho.php';
    require __DIR__.'/categoria.php';
    require __DIR__.'/cor.php';
    require __DIR__.'/estoque.php';

    $router->get('/',        "ViewController@index");

    $router->get('index',    "ProdutoController@index");
    $router->get('paginate', "ProdutoController@paginate");
    $router->get('{id}',     "ProdutoController@show");

    $router->post('/',       "ProdutoController@create");
    $router->put('{id}',     "ProdutoController@edit");
    $router->delete('{id}',  "ProdutoController@destroy");

});
