<?php

$router->group(['prefix' => 'tamanho'], function($router) {

    $router->get('/',        "ProdutoTamanhoController@index");
    $router->get('paginate', "ProdutoTamanhoController@paginate");
    $router->get('{id}',     "ProdutoTamanhoController@show");

    $router->post('/',       "ProdutoTamanhoController@create");
    $router->put('{id}',     "ProdutoTamanhoController@edit");
    $router->delete('{id}',  "ProdutoTamanhoController@destroy");

});
